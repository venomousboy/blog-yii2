<?php

namespace app\dto;

use yii;
use yii\helpers\ArrayHelper;
use app\models\User;

class DtoPost
{
    public function make($post)
    {
        return ArrayHelper::toArray($post, [
            'app\models\Post' => [
                'id',
                'title',
                'text',
                'created_at' => function ($post) {
                    return date('H:i d.m.Y', $post->created_at);
                },
                'updated_at' => function ($post) {
                    return date('H:i d.m.Y', $post->updated_at);
                },
                'username' => function ($post) {
                    return User::findOne($post->user_id)['username'];
                },
                'email' => function ($post) {
                    return User::findOne($post->user_id)['email'];
                },
                'is_author' => function ($post) {
                    return !Yii::$app->user->isGuest ?
                        $post->user_id == Yii::$app->user->identity->getId() :
                        false;
                },
                'is_admin' => function () {
                    return !Yii::$app->user->isGuest ?
                        Yii::$app->user->identity->role === User::ROLE_ADMIN :
                        false;
                },
                'count_comments' => function ($post) {
                    return count($post->comments);
                },
                'comments' => function ($post) {
                    $comments = [];
                    if (count($post->comments)) {
                        foreach ($post->comments as $comment) {
                            $comments[] = [
                                'id' => $comment->id,
                                'text' => $comment->text,
                                'created_at' => date('H:i d.m.Y', $comment->created_at),
                                'updated_at' => date('H:i d.m.Y', $comment->updated_at),
                                'username' => User::findOne($comment->user_id)['username'],
                                'email' => User::findOne($comment->user_id)['email'],
                                'is_author' => !Yii::$app->user->isGuest ?
                                    $comment->user_id == Yii::$app->user->identity->getId() :
                                    false,
                                'is_admin' => !Yii::$app->user->isGuest ?
                                    Yii::$app->user->identity->role === User::ROLE_ADMIN :
                                    false,
                            ];
                        }
                    }
                    return $comments;
                }
            ],
        ]);
    }
}