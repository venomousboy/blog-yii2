<?php

namespace app\dto;

use yii;
use yii\helpers\ArrayHelper;
use app\models\User;

class DtoComment
{
    public function make($comment)
    {
        return ArrayHelper::toArray($comment, [
            'app\models\Comment' => [
                'id',
                'text',
                'created_at' => function ($comment) {
                    return date('H:i d.m.Y', $comment->created_at);
                },
                'updated_at' => function ($comment) {
                    return date('H:i d.m.Y', $comment->updated_at);
                },
                'username' => function ($comment) {
                    return User::findOne($comment->user_id)['username'];
                },
                'email' => function ($comment) {
                    return User::findOne($comment->user_id)['email'];
                },
                'is_author' => function ($comment) {
                    return !Yii::$app->user->isGuest ?
                        $comment->user_id == Yii::$app->user->identity->getId() :
                        false;
                },
                'is_admin' => function () {
                    return !Yii::$app->user->isGuest ?
                        Yii::$app->user->identity->role === User::ROLE_ADMIN :
                        false;
                },
            ],
        ]);
    }
}