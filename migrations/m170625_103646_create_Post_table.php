<?php

use yii\db\Migration;

/**
 * Handles the creation of table `Post`.
 */
class m170625_103646_create_Post_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('Post', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'text' => $this->text()->notNull(),
            'created_at' => $this->integer(11)->notNull()->unsigned(),
            'updated_at' => $this->integer(11)->notNull()->unsigned(),
            'user_id' => $this->integer(11)->notNull(),
        ]);

        $this->createIndex(
            'idx-post-user_id',
            'Post',
            'user_id'
        );

        $this->addForeignKey(
            'fk-post-user_id',
            'Post',
            'user_id',
            'User',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-post-user_id',
            'Post'
        );

        $this->dropIndex(
            'idx-post-user_id',
            'Post'
        );

        $this->dropTable('Post');
    }
}
