<?php

use yii\db\Migration;

/**
 * Handles the creation of table `User`.
 */
class m170625_102602_create_User_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('User', [
            'id' => $this->primaryKey(),
            'username' => $this->string(255)->notNull(),
            'password' => $this->string(255)->notNull(),
            'email' => $this->string(255)->notNull(),
            'status' => "ENUM('active', 'non_active') NOT NULL",
            'role' => "ENUM('admin', 'user') NOT NULL",
            'auth_key' => $this->string(255)->notNull(),
            'created_at' => $this->integer(11)->notNull()->unsigned(),
            'updated_at' => $this->integer(11)->notNull()->unsigned(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('User');
    }
}
