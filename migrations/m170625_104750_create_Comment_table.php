<?php

use yii\db\Migration;

/**
 * Handles the creation of table `Comment`.
 */
class m170625_104750_create_Comment_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('Comment', [
            'id' => $this->primaryKey(),
            'text' => $this->text()->notNull(),
            'created_at' => $this->integer(11)->notNull()->unsigned(),
            'updated_at' => $this->integer(11)->notNull()->unsigned(),
            'post_id' => $this->integer(11)->notNull()->unsigned(),
            'user_id' => $this->integer(11)->notNull()->unsigned(),
        ]);

        $this->createIndex(
            'idx-comment-post_id',
            'Comment',
            'post_id'
        );

        $this->addForeignKey(
            'fk-comment-post_id',
            'Comment',
            'post_id',
            'Post',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-comment-user_id',
            'Comment',
            'user_id'
        );

        $this->addForeignKey(
            'fk-comment-user_id',
            'Comment',
            'user_id',
            'User',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-comment-post_id',
            'Comment'
        );

        $this->dropIndex(
            'idx-comment-post_id',
            'Comment'
        );

        $this->dropForeignKey(
            'fk-comment-user_id',
            'Comment'
        );

        $this->dropIndex(
            'idx-comment-user_id',
            'Comment'
        );

        $this->dropTable('Comment');
    }
}
