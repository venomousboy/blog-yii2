$( document ).ready(function() {
    //Загрузка постов с комментами
    var posts = ajaxRequest("/post/getall",
        "POST", {
            post: 'all'
        }, function(response) {
            $('#errors').remove();
            if (typeof response.error !== 'undefined') {
                showErrors(response.error);
            } else {
                response.forEach(function (el, post_i) {
                    addPost(el, post_i);
                });
            }
        }
    );

    //Обновляем капчу у комментов при появлени/исчезновении модального окна
    $('#modalComment').on('shown.bs.modal', function () {
        $('#comment_img_captcha')
            .attr('src', 'comment/captcha?id=' + Math.random());
    });
    $('#modalComment').on('hide.bs.modal', function () {
        $('.img_captcha')
            .attr('src', 'comment/captcha?id=' + Math.random());
    });
});

var generateUrl = function(text)
{
    var words = text.split(' '),
        url = /[a-z0-9~_\-\.]+\.[a-z]{2,9}(\/|:|\?[!-~]*)?$/i;
        res = '';
    words.forEach(function (word, i) {
        if (url.test(word)) {
            var https = /(^https?:\/\/)/,
                http = /(^http?:\/\/)/;
            if ((https.test(word))) {
                res += '<a href="https://' + word + '" target="_blank">' + word + '</a> ';
            } else if ((http.test(word))) {
                res += '<a href="http://' + word + '" target="_blank">' + word + '</a> ';
            } else {
                res += '<a href="http://' + word + '" target="_blank">' + word + '</a> ';
            }
        } else {
            res += word + ' ';
        }
    });
    return res;
}

var ajaxRequest = function(url, method, params, cb)
{
    $.ajax({
        type: method,
        url: url,
        data: params,
        success: function (response) {
            cb(JSON.parse(response));
        }
    });
}

var showErrors = function (errors)
{
    $('.posts').prepend('<div id="errors"><h3>Ошибка: </h3></div>');
    if (typeof errors === 'string') {
        $('#errors').append(errors);
    } else {
        errors.forEach(function (error, i) {
            $('#errors').append('<p>'+error+'</p>');
        });
    }
}

var toggleComments = function(index)
{
    event.preventDefault();
    var hidden = $('.show_post_comments_'+index).find('.comment');
    if (typeof hidden.attr('hidden') == 'undefined') {
        hidden.attr('hidden', true);
    } else {
        hidden.removeAttr('hidden');
    }
}

var createUpdatePost = function ()
{
    event.preventDefault();
    var csrfParam = $('meta[name="csrf-param"]').attr("content");
    var csrfToken = $('meta[name="csrf-token"]').attr("content");

    //Для изменения Поста
    if ($('#id').val() !== '') {
        var url = "/post/update",
            params = {
                Post: {
                    title: $('#title').val(),
                    text: $('#text').val(),
                    captcha: $('#text_captcha').val(),
                    id: $('#id').val()
                },
                csrfParam : csrfToken
            },
            action = 'update';
    //Для создания Поста
    } else {
        var url = "/post/create",
            params = {
                Post: {
                    title: $('#title').val(),
                    text: $('#text').val(),
                    captcha: $('#text_captcha').val()
                },
                csrfParam : csrfToken
            },
            action = 'create';
    }
    var post = ajaxRequest(url, "POST", params, function (response) {
        $('#errors').remove();
        if (typeof response.error !== 'undefined') {
            showErrors(response.error);
        } else {
            if (action === 'create') {
                var post_i = $('.post').size();
                addPost(response, post_i);
            } else {
                updatePost(response);
            }
        }
    });
}

var addPost = function (post, index)
{
    var html = '<div class="post">' +
        '<div class="id" id="post_' + post.id + '" hidden>' + post.id + '</div>' +
        '<div class="meta post_meta">' +
            '<div class="author">' + post.username + ' (' + post.email + ')</div>' +
            '<div class="date">' + post.created_at + '</div>' +
            '<div class="edit_post"></div>' +
        '</div>' +
        '<div class="post_content">' +
            '<h2>' + post.title + '</h2>' +
            '<p>' + generateUrl(post.text) + '</p>' +
        '</div>' +
        '<div class="comments show_post_comments_' + index + '">' +
            '<h4><a href="#" class="countComments" onclick="toggleComments(' + index + ')">Комментарии к посту <span>' + post.count_comments + '</span></a></h4>' +
            '<form role="form">' +
                '<div class="form-group">' +
                    '<label>Текст</label>' +
                    '<textarea type="text" class="form-control"></textarea>' +
                '</div>' +
                '<div class="form-group">' +
                    '<img class="img_captcha" src="comment/captcha">&nbsp;' +
                    '<div class="btn btn-default reload-captcha">' +
                        '<i class="glyphicon glyphicon-refresh"></i> Обновить' +
                    '</div>' +
                    '<input name="captcha" type="text" class="form-control text_captcha" placeholder="Пожалуйста, введите указанный на изображении код"/>' +
                '</div>' +
                '<button type="submit" class="btn btn-default" onclick="createUpdateComment(' + index + ')">Создать</button>' +
            '</form><br>' +
        '</div>' +
        '</div>';
    $('.posts > span').prepend(html);

    if (post.is_admin || post.is_author) {
        $('.comments.show_post_comments_' + index)
            .parent()
            .find('.edit_post')
            .prepend('<a href="#" onclick="removePost(' + index + ')">Удалить</a>')
            .prepend('<a href="#" onclick="editPost(' + index + ')" data-toggle="modal" data-target="#modalPost">Редактировать</a> | ');
    }

    if (post.comments.length > 0) {
        post.comments.forEach(function (comment, comment_i) {
            showComment(comment, index);
        });
    }
}

var editPost = function (i)
{
    clearPost();
    var title = $('.comments.show_post_comments_' + i)
        .parent()
        .find('h2')
        .html();
    var text = $('.comments.show_post_comments_' + i)
        .parent()
        .find('p')
        .html();
    var id = $('.comments.show_post_comments_' + i)
        .parent()
        .find('.id')
        .html();
    $('#title').val(title);
    $('#text').val(text);
    $('#id').val(id);
    $('#save').text('Изменить');
    $('#modalLabel').text('Изменить публикацию');
}

var updatePost = function (res)
{
    $('#post_'+res.id)
        .parent()
        .find('.post_content')
        .find('h2')
        .html(res.title);
    $('#post_'+res.id)
        .parent()
        .find('.post_content')
        .find('p')
        .html(res.text);
    $('#post_'+res.id)
        .parent()
        .find('.post_meta')
        .find('.date')
        .html(res.updated_at);
}

var removePost = function (i)
{
    var result = confirm("Хотите удалить?");
    if (result) {
        event.preventDefault();
        var csrfParam = $('meta[name="csrf-param"]').attr("content");
        var csrfToken = $('meta[name="csrf-token"]').attr("content");
        var id = $('.comments.show_post_comments_' + i)
            .parent()
            .find('.id')
            .html();

        var url = "/post/delete",
            params = {
                Post: {
                    id: id
                },
                csrfParam : csrfToken
            };
        var post = ajaxRequest(url, "POST", params, function (response) {

            $('#errors').remove();
            if (typeof response.error !== 'undefined') {
                showErrors(response.error);
            } else {
                $('.comments.show_post_comments_' + i)
                    .parent()
                    .remove();
            }
        });
    }
}

var clearPost = function ()
{
    $('#title').val('');
    $('#text').val('');
    $('#text_captcha').val('');
    $('#id').val('');
    $('#save').text('Сохранить');
    $('#modalLabel').text('Создать публикацию');
    $('#img_captcha').attr('src', 'post/captcha?id=' + Math.random());
}

var createUpdateComment = function (i)
{
    event.preventDefault();
    var csrfParam = $('meta[name="csrf-param"]').attr("content");
    var csrfToken = $('meta[name="csrf-token"]').attr("content");

    //Для изменения коммента
    if (typeof i === 'undefined') {
        var comment_id = $('#comment_id').val(),
            text = $('#comment_text').val(),
            captcha = $('#comment_text_captcha').val(),
            url = "/comment/update",
            params = {
                Comment: {
                    text: text,
                    comment_id: comment_id,
                    captcha: captcha
                },
                csrfParam : csrfToken
            },
            action = 'update';
    //Для создания коммента
    } else {
        var post_id = $('.comments.show_post_comments_' + i)
                .parent()
                .find('.id')
                .html(),
            text = $('.comments.show_post_comments_' + i)
                .find('textarea')
                .val(),
            captcha = $('.comments.show_post_comments_' + i)
                .find('.text_captcha')
                .val(),
            url = "/comment/create",
            params = {
                Comment: {
                    text: text,
                    post_id: post_id,
                    captcha: captcha
                },
                csrfParam : csrfToken
            },
            action = 'create';
    }

    var comment = ajaxRequest(url, "POST", params, function (response) {
        $('#errors').remove();
        if (typeof response.error !== 'undefined') {
            showErrors(response.error);
        } else {
            if (action === 'create') {
                addComment(response, i);
            } else {
                updateComment(response);
            }
        }
    });
}

var addComment = function (comment, post_i)
{
    showComment(comment, post_i);
    $('.show_post_comments_'+post_i)
        .find('.comment')
        .removeAttr('hidden');
    //Очищаем textarea
    $('.show_post_comments_'+post_i)
        .find('textarea')
        .val('');
    //Счетчик
    var cntComments =
    $('.show_post_comments_'+post_i)
        .find('.countComments')
        .find('span')
        .html();
    $('.show_post_comments_'+post_i)
        .find('.countComments')
        .find('span')
        .html(parseInt(cntComments) + 1);
    //Очищаем капчу
    $('.show_post_comments_' + post_i)
        .find('.text_captcha')
        .val('');
    //Обновляем капчу
    $('.img_captcha')
        .attr('src', 'comment/captcha?id=' + Math.random());
}

var showComment = function (comment, post_i)
{
    var newComment = '<div class="comment" hidden id="comment_' + comment.id + '">' +
        '<span class="comment_' + comment.id + '" hidden>' + comment.id + '</span>' +
        '<span class="post_i" hidden>' + post_i + '</span>' +
        '<div class="meta comment_meta">' +
            '<div class="author">' + comment.username + ' (' + comment.email + ')</div>' +
            '<div class="date">' + comment.created_at + '</div>' +
            '<div class="edit_comment"></div>' +
        '</div>' +
        '<p class="comment_message">' + generateUrl(comment.text) + '</p>' +
        '</div>';
    $('.comments.show_post_comments_' + post_i)
        .append(newComment);

    if (comment.is_admin || comment.is_author) {
        $('#comment_'+comment.id)
            .find('.edit_comment')
            .prepend('<a href="#" onclick="removeComment(' + comment.id + ')">Удалить</a>')
            .prepend('<a href="#" onclick="editComment(' + comment.id + ')" data-toggle="modal" data-target="#modalComment">Редактировать</a> | ');
    }
}

var editComment = function (id)
{
    clearComment();
    var text = $('#comment_' + id)
        .find('.comment_message')
        .html();
    $('#comment_text').val(text);
    $('#comment_id').val(id);
}

var updateComment = function (res)
{
    $('#comment_'+res.id)
        .find('.comment_message')
        .html(res.text);
    $('#comment_'+res.id)
        .find('.comment_meta')
        .find('.date')
        .html(res.updated_at);
}

var removeComment = function (i)
{
    var result = confirm("Хотите удалить?");
    if (result) {
        event.preventDefault();
        var csrfParam = $('meta[name="csrf-param"]').attr("content");
        var csrfToken = $('meta[name="csrf-token"]').attr("content");
        var id = $('.comment_' + i).html();

        var url = "/comment/delete",
            params = {
                Comment: {
                    id: id
                },
                csrfParam : csrfToken
            };
        var comment  = ajaxRequest(url, "POST", params, function (response) {

            $('#errors').remove();
            if (typeof response.error !== 'undefined') {
                showErrors(response.error);
            } else {
                var cntComments =
                    $('#comment_' + i)
                        .parent()
                        .find('.countComments')
                        .find('span')
                        .html();
                $('#comment_' + i)
                    .parent()
                    .find('.countComments')
                    .find('span')
                    .html(parseInt(cntComments) - 1);
                $('#comment_' + i).remove();
            }
        });
    }
}

var clearComment = function ()
{
    $('#comment_text').val('');
    $('#comment_text_captcha').val('');
    $('#comment_id').val('');
}

