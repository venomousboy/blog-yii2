<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2-comments-homework',
    'username' => 'username',
    'password' => 'password',
    'charset' => 'utf8',
];
