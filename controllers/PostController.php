<?php

namespace app\controllers;

use Yii;
use app\models\Post;
use app\models\PostSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\helpers\PermissionHelper;
use app\helpers\ViewJsonHelper;
use app\helpers\CaptchaHelper;
use app\services\PostService;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends Controller
{
    protected $entityService;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'matchCallback' => function($rule, $action) {
                            return PermissionHelper::requireAdmin();
                        }
                    ],
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'matchCallback' => function($rule, $action) {
                            return PermissionHelper::requireUser();
                        }
                    ],
                    [
                        'actions' => ['getall'],
                        'allow' => false,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function __construct($id, $module, PostService $entityService, $config = [])
    {
        $this->entityService = $entityService;
        parent::__construct($id, $module, $config);
    }

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Post model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionGetall()
    {
        if (Yii::$app->request->post()) {
            $posts = $this->entityService->findAll();
            ViewJsonHelper::json($posts);
        }
    }

    public function actionCreate()
    {
        if ($request = Yii::$app->request->post()) {
            $post = $this->entityService->save($request);
            ViewJsonHelper::json($post);
        }
    }

    public function actionUpdate()
    {
        if ($request = Yii::$app->request->post()) {
            $id = (int) $request['Post']['id'];
            unset($request['Post']['id']);
            $post = $this->entityService->update($id, $request);
            ViewJsonHelper::json($post);
        }
    }

    public function actionDelete()
    {
        if ($request = Yii::$app->request->post()) {
            if (!isset($request['Post']['id'])) {
                $post = ['error' => 'Вы не можете удалить публикацию'];
            }else {
                $id = (int) $request['Post']['id'];
                $post = $this->entityService->delete($id);
            }
            ViewJsonHelper::json($post);
        }
    }

    public function actionCaptcha()
    {
        $captcha = new CaptchaHelper();
        $captcha->generate('post');
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
