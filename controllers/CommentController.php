<?php

namespace app\controllers;

use Yii;
use app\models\Comment;
use app\models\CommentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\helpers\PermissionHelper;
use app\helpers\ViewJsonHelper;
use app\helpers\CaptchaHelper;
use app\services\CommentService;

/**
 * CommentController implements the CRUD actions for Comment model.
 */
class CommentController extends Controller
{
    protected $entityService;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'matchCallback' => function($rule, $action) {
                            return PermissionHelper::requireAdmin();
                        }
                    ],
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'matchCallback' => function($rule, $action) {
                            return PermissionHelper::requireUser();
                        }
                    ],
                    [
                        'actions' => [],
                        'allow' => false,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function __construct($id, $module, CommentService $entityService, $config = [])
    {
        $this->entityService = $entityService;
        parent::__construct($id, $module, $config);
    }

    /**
     * Lists all Comment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CommentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Comment model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        if ($request = Yii::$app->request->post()) {
            $comment = $this->entityService->save($request);
            ViewJsonHelper::json($comment);
        }
    }

    public function actionUpdate()
    {
        if ($request = Yii::$app->request->post()) {
            $id = (int) $request['Comment']['comment_id'];
            $comment = $this->entityService->update($id, $request);
            ViewJsonHelper::json($comment);
        }
    }

    public function actionDelete()
    {
        if ($request = Yii::$app->request->post()) {
            if (!isset($request['Comment']['id'])) {
                $comment = ['error' => 'Вы не можете удалить комментарий'];
            }else {
                $id = (int) $request['Comment']['id'];
                $comment = $this->entityService->delete($id);
            }
            ViewJsonHelper::json($comment);
        }
    }

    public function actionCaptcha()
    {
        $captcha = new CaptchaHelper();
        $captcha->generate('comment');
    }

    /**
     * Finds the Comment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Comment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Comment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
