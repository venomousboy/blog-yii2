<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form ActiveForm */
$this->title = 'Регистрация';

?>
<div class="post-content clearfix">
    <h1 class="page-title"><?= Html::encode($this->title) ?></h1>
    <hr>
    <?php if (Yii::$app->session->hasFlash('error')): ?>
        <p><?= Yii::$app->session->getFlash('error')?></p>>
        <hr>
    <?php endif; ?>
    <p><?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'username') ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
        <?= $form->field($model, 'email') ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-default btn-red']) ?>
    </div>
    <?php ActiveForm::end(); ?></p>

    <hr>

</div>
</div>

