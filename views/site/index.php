<?php

/* @var $this yii\web\View */
use yii\bootstrap\Html;
$this->title = 'Blog';
use yii\captcha\Captcha;
?>
<section class="content">
    <div class="wrapper">
        <div class="col-md-2"></div>
        <div class="col-md-8 posts">
            <?php if (!Yii::$app->user->isGuest): ?>
                <p>
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modalPost" onclick="clearPost()">Создать публикацию</button>
                </p>
                <!-- Modal -->
                <div class="modal fade" id="modalPost" tabindex="-10" role="dialog"
                     aria-labelledby="modalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title" id="modalLabel">
                                    Создать публикацию
                                </h4>
                            </div>

                            <!-- Modal Body -->
                            <div class="modal-body">

                                <form role="form">
                                    <div class="form-group">
                                        <label>Заголовок</label>
                                        <input type="text" id="id" hidden/>
                                        <input type="text" class="form-control"
                                               id="title" placeholder="Заголовок публикации..."/>
                                    </div>
                                    <div class="form-group">
                                        <label>Текст</label>
                                        <textarea type="text" class="form-control"
                                                  id="text"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <img id="img_captcha" src="post/captcha">&nbsp;
                                        <div id="reload_captcha" class="btn btn-default">
                                            <i class="glyphicon glyphicon-refresh"></i> Обновить
                                        </div>
                                        <input id="text_captcha" name="captcha"
                                               type="text" class="form-control"
                                               placeholder="Пожалуйста, введите указанный на изображении код"/>
                                    </div>
                                    <button type="submit" class="btn btn-default" data-dismiss="modal" onclick="createUpdatePost()" id="save">Создать</button>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="modalComment" tabindex="-10" role="dialog"
                     aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">
                                    Редактировать комментарий
                                </h4>
                            </div>

                            <!-- Modal Body -->
                            <div class="modal-body">

                                <form role="form">
                                    <div class="form-group">
                                        <label>Текст</label>
                                        <input type="text" id="comment_id" hidden/>
                                        <textarea type="text" class="form-control"
                                                  id="comment_text"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <img id="comment_img_captcha" src="comment/captcha">&nbsp;
                                        <div id="comment_reload_captcha" class="btn btn-default">
                                            <i class="glyphicon glyphicon-refresh"></i> Обновить
                                        </div>
                                        <input id="comment_text_captcha" name="captcha"
                                               type="text" class="form-control"
                                               placeholder="Пожалуйста, введите указанный на изображении код"/>
                                    </div>
                                    <button type="submit" class="btn btn-default" data-dismiss="modal" onclick="createUpdateComment()" id="edit">Изменить</button>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <span></span>
        </div>
    </div>
</section>
