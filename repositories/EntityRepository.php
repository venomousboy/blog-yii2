<?php

namespace app\repositories;

use Yii;
use app\models\User;

class EntityRepository
{
    protected function isEditable($model)
    {
        if (!Yii::$app->user->isGuest) {
            return Yii::$app->user->identity->role === User::ROLE_ADMIN ||
                $model->user_id == Yii::$app->user->identity->getId();
        }
        return false;
    }
}