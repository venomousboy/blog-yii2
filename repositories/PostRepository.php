<?php

namespace app\repositories;

use app\models\Post;
use app\helpers\CaptchaHelper;
use yii\web\NotFoundHttpException;
use yii\base\InvalidParamException;

class PostRepository extends EntityRepository implements EntityRepositoryInterface
{
    protected $postModel;

    public function __construct(Post $post)
    {
        $this->postModel = $post;
    }

    public function findById($id)
    {
        if (($model = $this->postModel::findOne($id)) !== null) {
            return $model;
        }
        throw new InvalidParamException('Данные не корректны');
    }

    public function findAll()
    {
        if (($posts = $this->postModel::find()
                ->with('comments')
                ->all ()) !== null) {
                    return $posts;
        }
        throw new NotFoundHttpException('Пока нет публикаций');
    }

    public function save($request)
    {
        if ($this->postModel->load($request) &&
            $this->postModel->validate()) {
            $this->postModel->save();
        }
        return $this->postModel;
    }

    public function deleteById($model)
    {
        if ($this->isEditable($model)) {
            $model->delete();
            return ['success' => true];
        }
        throw new InvalidParamException('Данные не корректны! Публикация не удалена');
    }

    public function update($model, $request)
    {
        if ($model->load($request) &&
            $this->isEditable($model)) {
            $model->save();
            return $model;
        }
        throw new InvalidParamException('Данные не корректны! Публикация не изменена');
    }

    public function checkCaptcha($request)
    {
        $captcha = new CaptchaHelper();
        if (!isset($request['Post']['captcha'])) {
            throw new InvalidParamException('Код не может быть пустым');
        }
        if ($captcha->check($request['Post']['captcha'], 'post')) {
            return true;
        }
        throw new InvalidParamException('Код не корректный');
    }
}