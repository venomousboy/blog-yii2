<?php

namespace app\repositories;

interface EntityRepositoryInterface
{
    public function findById($id);
    public function save($request);
    public function deleteById($model);
    public function update($model, $request);
    public function checkCaptcha($request);
}


