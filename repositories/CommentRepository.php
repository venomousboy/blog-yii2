<?php

namespace app\repositories;

use app\models\Comment;
use app\helpers\CaptchaHelper;
use yii\base\InvalidParamException;

class CommentRepository extends EntityRepository implements EntityRepositoryInterface
{
    protected $commentModel;

    public function __construct(Comment $comment)
    {
        $this->commentModel = $comment;
    }

    public function findById($id)
    {
        if (($model = $this->commentModel::findOne($id)) !== null) {
            return $model;
        }
        throw new InvalidParamException('Данные не корректны');
    }

    public function save($request)
    {
        if ($this->commentModel->load($request) &&
            $this->commentModel->validate()) {
            $this->commentModel->save();
        }
        return $this->commentModel;
    }

    public function deleteById($model)
    {
        if ($this->isEditable($model)) {
            $model->delete();
            return ['success' => true];
        }
        throw new InvalidParamException('Данные не корректны! Комментарий не удален');
    }

    public function update($model, $request)
    {
        if ($model->load($request) &&
            $this->isEditable($model)) {
            $model->save();
            return $model;
        }
        throw new InvalidParamException('Данные не корректны! Комментарий не изменен');
    }

    public function checkCaptcha($request)
    {
        $captcha = new CaptchaHelper();
        if (!isset($request['Comment']['captcha'])) {
            throw new InvalidParamException('Код не может быть пустым');
        }
        if ($captcha->check($request['Comment']['captcha'], 'comment')) {
            return true;
        }
        throw new InvalidParamException('Код не корректный');
    }
}