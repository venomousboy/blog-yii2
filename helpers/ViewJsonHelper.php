<?php

namespace app\helpers;

class ViewJsonHelper
{
    public static function json($response)
    {
        print json_encode($response, JSON_UNESCAPED_UNICODE);
    }
}