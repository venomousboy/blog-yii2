<?php

namespace app\helpers;
use Yii;
use app\models\User;

class PermissionHelper
{
    public static function requireAdmin()
    {
        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity->role === User::ROLE_ADMIN) {
                if (Yii::$app->user->identity->status === User::STATUS_ACTIVE)
                    return true;
                else
                    return false;
            }
        } else {
            return false;
        }
    }

    public static function requireUser()
    {
        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity->role === User::ROLE_USER) {
                if (Yii::$app->user->identity->status === User::STATUS_ACTIVE)
                    return true;
                else
                    return false;
            }
        } else {
            return false;
        }
    }
}