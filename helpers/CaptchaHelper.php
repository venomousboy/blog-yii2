<?php

namespace app\helpers;
use Yii;

class CaptchaHelper
{
    private $symbols = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcdefghijklmnopqrstuvwxyz';
    private $background = '/img/background.png';
    private $font = '/fonts/oswald.ttf';
    private $session;

    public function __construct()
    {
        $this->session = Yii::$app->session;
    }

    public function generate($name)
    {
        $captcha = substr(str_shuffle($this->symbols), 0, 6);
        $this->session->set($name, $captcha);
        $image = imagecreatefrompng(realpath(Yii::$app->request->BaseUrl) . $this->background);
        $colour = imagecolorallocate($image, 200, 240, 240);
        $font = realpath(Yii::$app->request->BaseUrl) . $this->font;
        $rotate = rand(-10, 10);
        imagettftext($image, 18, $rotate, 28, 32, $colour, $font, $this->session->get($name));
        header('Content-type: image/png');
        imagepng($image);
    }

    public function check($captcha, $name)
    {
        return $this->session->get($name) === $captcha ?: false;
    }
}