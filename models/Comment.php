<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "Comment".
 *
 * @property integer $id
 * @property string $text
 * @property string $created_at
 * @property string $updated_at
 * @property integer $post_id
 * @property integer $user_id
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'required'],
            [['text'], 'string'],
            [['text'], 'match', 'pattern'=>'/^[\x{0400}-\x{04FF}a-z\-\d\:\.\,\!\?\/\s]+$/ui', 'message' => '{attribute} '. Yii::t('app', 'не должен содержать спец. символы')],
            [['post_id', 'user_id', 'created_at', 'updated_at'], 'integer'],
            ['user_id', 'default', 'value' => Yii::$app->user->id, 'on' => 'default'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Текст',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
            'post_id' => 'Пост',
            'user_id' => 'Пользователь',
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => function() { return date('U'); },
            ],
        ];
    }
}
