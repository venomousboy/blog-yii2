<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "User".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property integer $status
 * @property string $role
 * @property string $auth_key
 * @property string $created_at
 * @property string $updated_at
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_NOT_ACTIVE = 'non_active'; //для не активированных юзеров
    const STATUS_ACTIVE = 'active'; //для активных юзеров

    const ROLE_ADMIN = "admin";
    const ROLE_USER = "user";

    public $password_hash;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'User';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password', 'email', 'role'], 'filter', 'filter' => 'trim'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['password', 'string', 'min' => 6, 'max' => 255],
            ['username', 'unique', 'targetClass' => User::className(), 'message' => 'Имя занято.'],
            ['email', 'email'],
            [['created_at', 'updated_at'], 'integer'],
            ['email', 'unique', 'targetClass' => User::className(), 'message' => 'E-mail занят.'],
            [['status'], 'in', 'range' => [User::STATUS_NOT_ACTIVE, User::STATUS_ACTIVE]],
            ['status', 'default', 'value' => User::STATUS_ACTIVE, 'on' => 'default'],
            ['role', 'in', 'range' => [User::ROLE_ADMIN, User::ROLE_USER]],
            ['role', 'default', 'value' => User::ROLE_USER, 'on' => 'default'],
            [['username', 'password', 'email', 'status', 'role'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'email' => 'Email',
            'status' => 'Status',
            'role' => 'Role',
            'auth_key' => 'Auth Key',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['user_id' => 'id']);
    }

    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['user_id' => 'id']);
    }

    /* Поведения */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => function() { return date('U'); },
            ],
        ];
    }

    /* Поиск */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    public static function getUsername($id)
    {
        return static::findOne(['id' => $id]);
    }

    /* Хелперы */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Аутентификация пользователя
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     * @return boolean if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function signup()
    {
        $user = new self();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->status = $this->status;
        $user->role = $this->role;
        $this->setPassword($this->password);
        $user->password = $this->password_hash;
        $user->generateAuthKey();

        return $user->save() ? $user : null;
    }
}
