<?php

namespace app\services;

use app\repositories\CommentRepository;
use app\dto\DtoComment;

class CommentService extends EntityService
{
    public function __construct(CommentRepository $commentRepository, DtoComment $dtoComment)
    {
        parent::__construct($commentRepository, $dtoComment);
    }
}