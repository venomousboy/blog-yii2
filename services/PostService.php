<?php

namespace app\services;

use app\repositories\PostRepository;
use app\dto\DtoPost;
use yii\web\NotFoundHttpException;

class PostService extends EntityService
{
    protected $dtoPost;

    public function __construct(PostRepository $postRepository, DtoPost $dtoPost)
    {
        parent::__construct($postRepository, $dtoPost);
        $this->dtoPost = $dtoPost;
    }

    public function findAll()
    {
        try {
            $posts = $this->entityRepository->findAll();
            return $this->dtoPost->make($posts);
        } catch (NotFoundHttpException $e) {
            return [
                'error' => $e->getMessage()
            ];
        }
    }
}