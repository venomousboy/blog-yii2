<?php

namespace app\services;

use Yii;
use app\repositories\EntityRepositoryInterface;
use yii\base\InvalidParamException;

class EntityService
{
    protected $entityRepository;
    protected $dtoEntity;

    public function __construct(EntityRepositoryInterface $entityRepository, $dtoEntity)
    {
        $this->entityRepository = $entityRepository;
        $this->dtoEntity = $dtoEntity;
    }

    public function save($request)
    {
        if (!Yii::$app->user->isGuest) {
            try {
                $this->entityRepository->checkCaptcha($request);
                $model = $this->entityRepository->save($request);
                if (count($model->getErrors())) {
                    $errors = [];
                    foreach ($model->getErrors() as $attr => $er) {
                        $errors[] = $er[0];
                    }
                    return ['error' => $errors];
                }
                return $this->dtoEntity->make($model);
            } catch (InvalidParamException $e) {
                return [
                    'error' => $e->getMessage()
                ];
            }
        }
        return ['error' => 'Авторизуйтесь, чтобы создавать публикации и комментировать'];
    }

    public function delete($id)
    {
        try {
            $model = $this->entityRepository->findById($id);
            return $this->entityRepository->deleteById($model);
        } catch (InvalidParamException $e) {
            return [
                'error' => $e->getMessage()
            ];
        }
    }

    public function update($id, $request)
    {
        try {
            $this->entityRepository->checkCaptcha($request);
            $model = $this->entityRepository->findById($id);
            $model = $this->entityRepository->update($model, $request);
            return $this->dtoEntity->make($model);
        } catch (InvalidParamException $e) {
            return [
                'error' => $e->getMessage()
            ];
        }
    }
}